<?php

namespace App\Controller;

use App\Entity\Bestellingregel;
use App\Form\BestellingregelType;
use App\Repository\BestellingregelRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/bestellingregel")
 */
class BestellingregelController extends AbstractController
{
    /**
     * @Route("/", name="bestellingregel_index", methods={"GET"})
     */
    public function index(BestellingregelRepository $bestellingregelRepository): Response
    {
        return $this->render('bestellingregel/index.html.twig', [
            'bestellingregels' => $bestellingregelRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="bestellingregel_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $bestellingregel = new Bestellingregel();
        $form = $this->createForm(BestellingregelType::class, $bestellingregel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($bestellingregel);
            $entityManager->flush();

            return $this->redirectToRoute('bestellingregel_index');
        }

        return $this->render('bestellingregel/new.html.twig', [
            'bestellingregel' => $bestellingregel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bestellingregel_show", methods={"GET"})
     */
    public function show(Bestellingregel $bestellingregel): Response
    {
        return $this->render('bestellingregel/show.html.twig', [
            'bestellingregel' => $bestellingregel,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="bestellingregel_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Bestellingregel $bestellingregel): Response
    {
        $form = $this->createForm(BestellingregelType::class, $bestellingregel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('bestellingregel_index');
        }

        return $this->render('bestellingregel/edit.html.twig', [
            'bestellingregel' => $bestellingregel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="bestellingregel_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Bestellingregel $bestellingregel): Response
    {
        if ($this->isCsrfTokenValid('delete'.$bestellingregel->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($bestellingregel);
            $entityManager->flush();
        }

        return $this->redirectToRoute('bestellingregel_index');
    }
}
