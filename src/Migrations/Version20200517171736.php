<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200517171736 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fruit ADD recept_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fruit ADD CONSTRAINT FK_A00BD297C6BF5295 FOREIGN KEY (recept_id) REFERENCES recept (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A00BD297C6BF5295 ON fruit (recept_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fruit DROP FOREIGN KEY FK_A00BD297C6BF5295');
        $this->addSql('DROP INDEX UNIQ_A00BD297C6BF5295 ON fruit');
        $this->addSql('ALTER TABLE fruit DROP recept_id');
    }
}
