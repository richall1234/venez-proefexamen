<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200517173516 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bestellingregel ADD recept_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE bestellingregel ADD CONSTRAINT FK_DA487844C6BF5295 FOREIGN KEY (recept_id) REFERENCES recept (id)');
        $this->addSql('CREATE INDEX IDX_DA487844C6BF5295 ON bestellingregel (recept_id)');
        $this->addSql('ALTER TABLE recept DROP FOREIGN KEY FK_B92268A18E33DAAA');
        $this->addSql('DROP INDEX IDX_B92268A18E33DAAA ON recept');
        $this->addSql('ALTER TABLE recept DROP bestellingregel_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE bestellingregel DROP FOREIGN KEY FK_DA487844C6BF5295');
        $this->addSql('DROP INDEX IDX_DA487844C6BF5295 ON bestellingregel');
        $this->addSql('ALTER TABLE bestellingregel DROP recept_id');
        $this->addSql('ALTER TABLE recept ADD bestellingregel_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE recept ADD CONSTRAINT FK_B92268A18E33DAAA FOREIGN KEY (bestellingregel_id) REFERENCES bestellingregel (id)');
        $this->addSql('CREATE INDEX IDX_B92268A18E33DAAA ON recept (bestellingregel_id)');
    }
}
