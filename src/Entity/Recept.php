<?php

namespace App\Entity;

use App\Repository\ReceptRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReceptRepository::class)
 */
class Recept
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $naam;

    /**
     * @ORM\Column(type="integer")
     */
    private $prijs;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bereidingswijze;

    /**
     * @ORM\OneToOne(targetEntity=Fruit::class, mappedBy="recept", cascade={"persist", "remove"})
     */
    private $fruit;

    /**
     * @ORM\OneToMany(targetEntity=Bestellingregel::class, mappedBy="recept")
     */
    private $bestellingregels;

    public function __construct()
    {
        $this->bestellingregels = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNaam(): ?string
    {
        return $this->naam;
    }

    public function setNaam(string $naam): self
    {
        $this->naam = $naam;

        return $this;
    }

    public function getPrijs(): ?int
    {
        return $this->prijs;
    }

    public function setPrijs(int $prijs): self
    {
        $this->prijs = $prijs;

        return $this;
    }

    public function getBereidingswijze(): ?string
    {
        return $this->bereidingswijze;
    }

    public function setBereidingswijze(string $bereidingswijze): self
    {
        $this->bereidingswijze = $bereidingswijze;

        return $this;
    }

    public function getFruit(): ?Fruit
    {
        return $this->fruit;
    }

    public function setFruit(?Fruit $fruit): self
    {
        $this->fruit = $fruit;

        // set (or unset) the owning side of the relation if necessary
        $newRecept = null === $fruit ? null : $this;
        if ($fruit->getRecept() !== $newRecept) {
            $fruit->setRecept($newRecept);
        }

        return $this;
    }

    /**
     * @return Collection|Bestellingregel[]
     */
    public function getBestellingregels(): Collection
    {
        return $this->bestellingregels;
    }

    public function addBestellingregel(Bestellingregel $bestellingregel): self
    {
        if (!$this->bestellingregels->contains($bestellingregel)) {
            $this->bestellingregels[] = $bestellingregel;
            $bestellingregel->setRecept($this);
        }

        return $this;
    }

    public function removeBestellingregel(Bestellingregel $bestellingregel): self
    {
        if ($this->bestellingregels->contains($bestellingregel)) {
            $this->bestellingregels->removeElement($bestellingregel);
            // set the owning side to null (unless already changed)
            if ($bestellingregel->getRecept() === $this) {
                $bestellingregel->setRecept(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->naam;
    }



}
