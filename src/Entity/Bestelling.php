<?php

namespace App\Entity;

use App\Repository\BestellingRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=BestellingRepository::class)
 */
class Bestelling
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $klant;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $telefoon;

    /**
     * @ORM\Column(type="datetime")
     */
    private $afhaaldatum;

    /**
     * @ORM\OneToMany(targetEntity=Bestellingregel::class, mappedBy="bestelling")
     */
    private $bestellingregels;

    public function __construct()
    {
        $this->bestellingregels = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKlant(): ?string
    {
        return $this->klant;
    }

    public function setKlant(string $klant): self
    {
        $this->klant = $klant;

        return $this;
    }

    public function getTelefoon(): ?string
    {
        return $this->telefoon;
    }

    public function setTelefoon(string $telefoon): self
    {
        $this->telefoon = $telefoon;

        return $this;
    }

    public function getAfhaaldatum(): ?\DateTimeInterface
    {
        return $this->afhaaldatum;
    }

    public function setAfhaaldatum(\DateTimeInterface $afhaaldatum): self
    {
        $this->afhaaldatum = $afhaaldatum;

        return $this;
    }

    /**
     * @return Collection|Bestellingregel[]
     */
    public function getBestellingregels(): Collection
    {
        return $this->bestellingregels;
    }

    public function addBestellingregel(Bestellingregel $bestellingregel): self
    {
        if (!$this->bestellingregels->contains($bestellingregel)) {
            $this->bestellingregels[] = $bestellingregel;
            $bestellingregel->setBestelling($this);
        }

        return $this;
    }

    public function removeBestellingregel(Bestellingregel $bestellingregel): self
    {
        if ($this->bestellingregels->contains($bestellingregel)) {
            $this->bestellingregels->removeElement($bestellingregel);
            // set the owning side to null (unless already changed)
            if ($bestellingregel->getBestelling() === $this) {
                $bestellingregel->setBestelling(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->klant;
    }
}
