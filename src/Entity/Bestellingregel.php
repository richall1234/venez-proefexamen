<?php

namespace App\Entity;

use App\Repository\BestellingregelRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Recept;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=BestellingregelRepository::class)
 */
class Bestellingregel
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $aantal;

    /**
     * @ORM\ManyToOne(targetEntity=Recept::class, inversedBy="bestellingregels")
     */
    private $recept;

    /**
     * @ORM\ManyToOne(targetEntity=Bestelling::class, inversedBy="bestellingregels")
     */
    private $bestelling;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAantal(): ?int
    {
        return $this->aantal;
    }

    public function setAantal(int $aantal): self
    {
        $this->aantal = $aantal;

        return $this;
    }

    public function getRecept(): ?Recept
    {
        return $this->recept;
    }

    public function setRecept(?Recept $recept): self
    {
        $this->recept = $recept;

        return $this;
    }

    public function getBestelling(): ?Bestelling
    {
        return $this->bestelling;
    }

    public function setBestelling(?Bestelling $bestelling): self
    {
        $this->bestelling = $bestelling;

        return $this;
    }

}
